package ru.gdgkazan.simpleweather.screen.weatherlist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.arturvasilov.sqlite.core.BasicTableObserver;
import ru.arturvasilov.sqlite.core.SQLite;
import ru.gdgkazan.simpleweather.R;
import ru.gdgkazan.simpleweather.data.model.City;
import ru.gdgkazan.simpleweather.data.tables.CityTable;
import ru.gdgkazan.simpleweather.data.tables.RequestTable;
import ru.gdgkazan.simpleweather.network.NetworkService;
import ru.gdgkazan.simpleweather.screen.general.LoadingDialog;
import ru.gdgkazan.simpleweather.screen.general.LoadingView;
import ru.gdgkazan.simpleweather.screen.general.SimpleDividerItemDecoration;
import ru.gdgkazan.simpleweather.screen.weather.WeatherActivity;

/**
 * @author Artur Vasilov
 */
public class WeatherListActivity extends AppCompatActivity implements CitiesAdapter.OnItemClick, BasicTableObserver {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    @BindView(R.id.empty)
    View mEmptyView;

    private CitiesAdapter mAdapter;
    private LoadingView mLoadingView;
    private List<City> cities;
    private City mCity;
    private List<City> loadedCity = new ArrayList<>();
    private static final String CITY_NAME_KEY = "city_name";
    private static final String WEATHER_KEY = "weather";

    @NonNull
    public static Intent makeIntent(@NonNull Activity activity, @NonNull String cityName){
         Intent intent = new Intent(activity, WeatherListActivity.class);
         intent.putExtra(CITY_NAME_KEY, cityName);
         return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_list);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(this, false));
        mAdapter = new CitiesAdapter(getInitialCities(), this);
        mRecyclerView.setAdapter(mAdapter);
        mLoadingView = LoadingDialog.view(getSupportFragmentManager());

        /**
         * TODO : task
         *
         * 1) Load all cities (pair id-name) if WeatherCityTable is empty from http://openweathermap.org/help/city_list.txt
         * and save them to database
         *   Нужно получить список городов http://openweathermap.org/help/city_list.txt и сохранить его в базу
         *   (Так же тут теперь работа с архивом).
         * 2) Using the information from the first step find id for each current city
         * 3) Load forecast in all cities using one single request http://openweathermap.org/current#severalid
         *      Получить информацию о погоде во всех городах одним запросом http://openweathermap.org/current#severalid
         * 4) Do all this work in NetworkService (you need to modify it to better support multiple requests)
         *      Вся работа должна выполняться в сервисе (его нужно доработать)
         * 5) Use SQLite API to manage communication
         * 6) Handle configuration changes
         * 7) Modify all the network layer to create a universal way for managing queries with pattern A

         * 8) Реализуйте обновление данных через SwipeRefreshLayout
         * 9) Реализуйте обработку пересоздания Activity
         */

        if (savedInstanceState==null || !savedInstanceState.containsKey(WEATHER_KEY)) {
            loadWeather();
        } else {
            showWeather();
        }
    }

    private void loadWeather (){
        mLoadingView.showLoadingIndicator();
        SQLite.get().registerObserver(RequestTable.TABLE, this);
        startService(new Intent(this, NetworkService.class));
    }

    @Override
    public void onItemClick(@NonNull City city) {
        startActivity(WeatherActivity.makeIntent(this, city.getName()));
    }

    @Override
    public void onTableChanged() {
        if (cities.isEmpty()) {
            mCity = null;
            showError();
        } else {
            mCity = cities.get(0);
            showWeather();
        }
        mLoadingView.hideLoadingIndicator();
        SQLite.get().unregisterObserver(this);
    }

    private void showError(){
        mLoadingView.hideLoadingIndicator();
        Snackbar snackbar = Snackbar.make(mRecyclerView, R.string.weather_error, Snackbar.LENGTH_LONG)
                .setAction(R.string.retry, view -> loadWeather());
        snackbar.setDuration(4000);
        snackbar.show();
    }

    private List<City> sortAllcities(List<City> cities){
        Collections.sort(cities, new Comparator<City>(){
            @Override
            public int compare(City o1, City o2){
                return o1.getName().compareTo(o2.getName());
            }
        });
        return cities;
    }

    @NonNull
    private List<City> getInitialCities() {
        List<City> cities = new ArrayList<>();
        String[] initialCities = getResources().getStringArray(R.array.initial_cities);
        for (String city : initialCities) {
            cities.add(new City(city));
        }
        return cities;
    }

    private void showWeather() {
        //формируем списочный массив
        loadedCity.add(mCity);

        if(loadedCity.size() >= cities.size()){
            sortAllcities(loadedCity);
            //передаем массив методу changeDataSet адаптера списка
            mAdapter.changeDataSet(loadedCity);
            //очищаем массив
            loadedCity.clear();
        }
    };
}
